---
layout: handbook-page-toc
title: "How the UX Research team operates at GitLab"
description: "How we decide what to research, spend our time, measure our success, and more"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## How we decide what to research
UX Researchers collaborate with Product Managers to determine the scope of research studies. Where possible, UX Researchers should try to attend planning meetings for their designated groups.
UX Researchers should proactively offer ways in which they can assist in the delivery of research. They should also suggest and discuss their own ideas for research studies with Product Managers.

## How we spend our time
UX Researchers have the following guidance on how they should be spending their time:

* **[<10% Solution Validation](/handbook/engineering/ux/ux-research-training/solution-validation-and-methods/)** - This translates to less than 10% of a researcher's time being allocated to assisting Product Designers and Product Design Managers with Solution Validation research.
* **[~60% Problem Validation](/handbook/engineering/ux/ux-research-training/problem-validation-and-methods/)** - Researchers spend more than half of their time working with Product Managers conducting Problem Validation research, with the long-term goal of investing their time towards training and mentoring.
* **[~30% Foundational/Strategic Research](/handbook/engineering/ux/ux-research-training/strategic-research-at-gitlab/)** - Ideally, 1 big project per researcher, per quarter. These are projects that feed into the researcher's stage/team, cross-stages, or section and may be derived by the researcher. Examples:
    * Go deeper into a SUS theme that is specific to the stage(s) they support. Doing this will help the team focus on issues that contribute to detractors.
    * Look deeper into performance (ex: Why is our performance perceived the way it is?)
    * Pain points - Are those known for each stage and/or group? Are they being dealt with? Are there real-life customer stories to help get those pain points addressed?
    * A project could even be derived from a Problem Validation topic or follow-up area.

Three noteworthy benefits to conducting Foundational/Strategic research:
1. Researchers gain subject matter knowledge in that topic
1. Researchers have an opportunity to impact Product and influence strategy
1. Career growth opportunities

## Milestones
Like other departments at GitLab, UX Researchers follow the [Product Development Timeline](/handbook/engineering/workflow/#product-development-timeline) and use milestones to schedule their work. Milestones change monthly ([find out the dates for upcoming milestones](https://gitlab.com/groups/gitlab-org/-/milestones)).

## How we work
1. We collaborate with Product Designers, Product Managers, and Engineers to collectively determine what areas to conduct research on.
1. We follow a [priortization process](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/research-prioritization/) that helps us distribute our time effectively across the research projects occurring within our stage groups.

### UX Research peer reviews
UX Researchers will frequently drive research projects themselves in close collaboration with Product and/or Design.  When this occurs, UX Researchers will take part in a peer review process on the test plan and the final output of the research.  There are many benefits to a peer review process: 
- **Visibility** - UX Researchers will be more informed about the research being conducted outside of their direct area of coverage.
- **Learning** - UX Researchers can learn from each other by reviewing the approaches being taken and the justification behind those approaches.
- **Quality** - Research deliverables will be more standardized and of higher quality as a result of a peer review.
- **Onboarding** - New UX Research hires will benefit by seeing standardized examples and learning about the quality bar to replicate.
- **Diversity** - We value diverse views and perceptions on how the content is being interpreted.
- **Mentoring** - UX Researchers will have more opportunities to mentor each other in their craft.

The UX Research peer review process is designed to be asynchonous and inclusive to all UX Researchers.  The process is as follows:

1. **Share** - A UX Researcher has either a test plan and/or a final report that needs to be peer reviewed.  **They ensure it's shareable and editable.**
1. **Post** - Within the _#ux_research_team_lounge_ Slack channel, they post a link to the test plan or final report, along with a request to review.  They include a date and time that they need feedback by.
     - Example: _"Hi - can I please request a review of this test plan (link)?  Feedback is needed by Thursday.  Thank you!"_
     - Best practice: Reviewers should aim to provide reviewers at least 24 hours to review.
1. **Review** - The reviewer provides feedback directly in the document, issue, etc.
1. **Inform** - When the review is complete, the reviewer responds to the thread and mentions the UX Researcher.
     - Example: _"@ name - your testplan has been reviewed.  Let me know if you have any questions!"_
1. **Edit** - The UX Researcher looks at the feedback, follows up on any questions, and makes any adjustments necessary.  
1. **Mark as done** - When complete, they put a green check mark emoji reaction on their original post, indicating that the peer review request has been completed and is closed.

UX Researchers are all required to take part in a peer review with their final reports that they created.  Additionally, it's strongly encouraged for UX Researchers to help their peers by reviewing anything submitted through the peer review process.

## How we socialize research findings
When we drive our own research projects, it means we're also responsible for socializing those insights.  The most effective way to do that is as follows:
- Create a brief into on what was done. (Example: _Hi team! I recently finished a tree test :deciduous_tree: on the left sidebar navigation focused in the Monitor, Infrastructure, Deployments, and Packages & Registries items with experienced GitLab users. I'm excited to share the key insights :star: :_)
- List 3-4 bullets with the key insights. These should not include any detail - just very short sentences on the high-level findings.
- Links to the: Research report, Video readout, Data sheet, Research issue, Dovetail project
- A 'Next steps' sections that includes bullets on what will be happening as a result of the research.  These should be links to issues that are actionable insights.
- The posting is shared out via Slack to _at least_ the three following channels: #ux, #ux_research, and #product.  It's advised to also share the posting in any relevant stage-group channels, too.

Below is an example of the formatting:

<img src=posting.png>

#### How problem validation research works
* [Problem Validation research for single-stage-group initiatives](/handbook/engineering/ux/ux-research-training/problem-validation-single-stage-group/)
* [Strategic Research (Problem Validation research for multi-stage-group initiatives)](/handbook/engineering/ux/ux-research-training/strategic-research-at-gitlab/)

#### How solution validation research works
[Solution validation research](/handbook/engineering/ux/ux-research-training/solution-validation-and-methods/) at GitLab is led by Product Designers, with support from Product Design Managers. Occasionally, Product Design Managers may need to escalate queries about solution validation research to UX Researchers for advice and feedback. 

If capacity allows, UX Researchers can help with solution validation research efforts.
Product Managers and Product Designers follow the steps in the [Validation phase 4](/handbook/product-development-flow/#validation-phase-4-solution-validation) when planning and executing solution validation research. 

## How to request research
Any GitLab Team Member can open a research request. If you are **not** a Product Manager, Product Designer, or UX Researcher, please open a Research request issue using the available template in the [UX Research project](https://gitlab.com/gitlab-org/ux-research/). Once completed, please assign the issue to the relevant [Product Manager](/handbook/product/categories/), [Product Designer](/handbook/product/categories/), and [UX Researcher](https://about.gitlab.com/company/team/?department=ux-research-team). The UX researcher assigned to the issue  will review it and notify you when/if they plan to proceed with the work.


