---
layout: markdown_page
title: Handling trials and extensions for GitLab Self-Managed
category: GitLab Self-Managed licenses
description: Issuing a license to "extend" Self-managed trials and grace periods
---

{:.no_toc}

----

## Overview

Self-managed trials and grace periods cannot be extended - a license must be re-issued and applied
to the instance in order to "extend" a trial.

Requests for Grace period extensions, Temporary keys, Temporary extensions, Temporary licenses, 
and Trial extensions all require generating of a Trial License.

> **NOTE**: Non-trial licenses are required to match an existing subscription and these licenses 
generally have a span of 1 year. There is an 
[ongoing discussion](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/3817) on
how to support complex subscription models.

Support tries as much as possible to refrain from issuing non-trial licenses.
We are allowed to issue trial licenses because they:

- Have no grace period
- Don't affect historical data tracking
- Aren't tied to any subscriptions for revenue purposes

### Approval required for trials longer than 30 days

For trial license requests where the `Expiry Date` is more than 30 calendar days from the Start date, a **Sales Manager** approval is required. When asking for approval, also consider the time between waiting for approval and the license period.
   - For example, if the requested trial license is for 33 days, and the wait time for approval could be up to 5 days. By the time the approval is given, perhaps the request doesn't need an Approval in the first place.
   - Ask the requestor to CC a manager for approval and set the ticket status to `On Hold`.
> If you notice several recent requests for temporary keys, consult with a support manager on how to proceed.

### How to create a trial license

To create a new trial license, follow these steps:

1. Set the `License type` to `Legacy License`.
1. Delete the contents of `Customer` field if present.
1. Delete the contents of `Zuora subscription ID` field if present.
1. Set the `Users count` number to what is requested.
1. Delete the contents of `Previous users count` field if present.
1. Delete the contents of `Trueup count` field if present.
1. Set the `Plan code` to what is requested.
1. Ensure the `Trial` checkbox is checked. 
1. Set `Starts at` to today's date.
1. Set `Expires at` to the requested date. See note above for [required approvals](#approval-required-for-trials-longer-than-30-days).
1. Set `Notes` to the ticket or issue URL.
1. Click `Save`. The license will be automatically sent to the email specified in the `Email` field.

If you need to [send a trial license to another contact](/handbook/support/license-and-renewals/workflows/self-managed/sending_license_to_different_email.html#overview), 
use the `Forward license email` tab after saving the new license.

### Emergency Weekend Licenses

If you're on call and you need a license generated and you don't have access to the customers dot interface use the [emergency license generator](https://gitlab-com.gitlab.io/support/toolbox/forms_processor/LR/license_creation_emergency.html)
The customer email is where the license will be sent and the User count is how many users the license will have. All of the licenses generated through this method will have 10 days.

